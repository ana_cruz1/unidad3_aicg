<x-app-layout>
    <x-slot name="header">
        
    </x-slot>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>About</title>

  <!-- Font Awesome Icons -->
  <link href="{{asset('css/all.min.css')}}" rel="stylesheet" type="text/css">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
  <script src="https://kit.fontawesome.com/87f8d9e4cd.js" crossorigin="anonymous"></script>
  <!-- Plugin CSS -->
  <link href="{{asset('css/magnific-popup.css')}}" rel="stylesheet">

  <!-- Theme CSS - Includes Bootstrap -->
  <link href="{{asset('css/creative.min.css')}}" rel="stylesheet">
</head>

<body id="page-top">
  <!-- About Section -->
  <section class="page-section bg-primary" id="about">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8 text-center">
          <h2 class="text-white mt-0">About The Project</h2>
          <br></br>
          <p class="text-white-50 mb-4">Proyecto de la material: Desarrollo Web Profesional, perteneciente a la unidad 3. Trabajo en equipo con las herramientas: GitLab y sus respectivos issues de tareas asignadas. Y con los lenguajes de programación: Laravel, php, html, css, bootstrap.</p>
        </div>
      </div>
    </div>
  </section>

  <!-- Services Section -->
  <section class="page-section" id="services">
    <div class="container">
      <h2 class="text-center mt-0">Development Team</h2>
      <div class="row">
        <div class="col-lg-4 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-gem text-primary mb-4"></i>
            <h3 class="h4 mb-2">Ana Cruz</h3>
            <p class="text-muted mb-0">Desarrollo y programación de los issues: US0, US1 y US2.</p>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-gem text-primary mb-4"></i>
            <h3 class="h4 mb-2">Alexia Garcia</h3>
            <p class="text-muted mb-0">Desarrollo y programación de los issues: US3, US4 y US5.</p>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 text-center">
          <div class="mt-5">
            <i class="fas fa-4x fa-gem text-primary mb-4"></i>
            <h3 class="h4 mb-2">Maybe Herrera</h3>
            <p class="text-muted mb-0">Desarrollo y programación de los issues: US6 y US20.</p>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- Footer -->
  <footer class="bg-light py-5">
    <div class="container">
      <div class="small text-center text-muted">Copyright &copy; 2021 - UTHermosillo Ing. Desarrollo y Gestión de Software</div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="{{asset('js/jquery.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>

  <!-- Plugin JavaScript -->
  <script src="{{asset('js/jquery.easing.min.js')}}"></script>
  <script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>

  <!-- Custom scripts for this template -->
  <script src="{{asset('js/creative.min.js')}}"></script>

</body>
</html>

</x-app-layout>