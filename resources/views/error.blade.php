<x-app-layout>
    <x-slot name="header">
        
    </x-slot>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>About</title>

  <!-- Font Awesome Icons -->
  <link href="{{asset('css/all.min.css')}}" rel="stylesheet" type="text/css">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
  <script src="https://kit.fontawesome.com/87f8d9e4cd.js" crossorigin="anonymous"></script>
  <!-- Plugin CSS -->
  <link href="{{asset('css/magnific-popup.css')}}" rel="stylesheet">

  <!-- Theme CSS - Includes Bootstrap -->
  <link href="{{asset('css/creative.min.css')}}" rel="stylesheet">
</head>

<body id="page-top">
    <section class="page-section bg-primary" id="about">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8 text-center">
          <h2 class="text-white mt-0">Error 404, Página no encontrada.</h2>
          <br></br>
          <p class="text-white-50 mb-4">Lo sentimos, la página que se intenta ingresar no esta disponible en este momento.</p>
        </div>
      </div>
    </div>
  </section>
</body>

<footer class="bg-light py-5">
    <div class="container">
      <div class="small text-center text-muted">Copyright &copy; 2021 - UTHermosillo Ing. Desarrollo y Gestión de Software</div>
    </div>
  </footer>
</html>

</x-app-layout>